<?php
/**
 * This is the menubuilder main configuration file. 
 * To register new templates (presenters), add them to the list below in the templates section.
 * Keep in mind that the name you give to it will be used to reference the themplate in the .env file.
 */
return [
    'name' => 'Menubuilder',
    'providers' => [
        \Modules\Menubuilder\Providers\MenubuilderServiceProvider::class,
    ],
    'aliases' => [
        "Menubuilder\Menu"=> \Modules\Menubuilder\Entities\Menu::class,
        "Menubuilder\MenuItem"=> \Modules\Menubuilder\Entities\MenuItem::class,
        "Menubuilder\MenuItemType"=> \Modules\Menubuilder\Entities\MenuItemType::class,
    ],
    #The template list. Register new templates here.
    'templates'=> [
        "bootstrap"=> \Modules\Menubuilder\Presenters\Menu\Bootstrap\NavBootstrapMenuPresenter::class,
        "bootstrapcustom"=> \Modules\Menubuilder\Presenters\Menu\Bootstrap\CustomThemeMenuPresenter::class,
    ]
];
