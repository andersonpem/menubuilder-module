<?php
namespace Modules\Menubuilder\Presenters\Menu\Bootstrap;

use Laracasts\Presenter\Presenter;
use Modules\Menubuilder\Presenters\Menu\MenuPresenter;
/**
 *  This class implements the Menu Presenter using bootstrap wrapping.
 *  The class renders an UL of class "nav navbar-nav"
 *  The <nav></nav> structure can be implemented by the developer, since only the menu options are in fact rendered here.
 *  To create custom classes with other implementations, copy this class and rename it.
 *  This class was partly inspired by nwidart/laravel-menus and also spatie menus components.
 *  Although most of the internal logic is self made, the function names were very clear :)
 *  And we all know how hard it is to name things in computing ^^
 */
class CustomThemeMenuPresenter extends MenuPresenter
{
    public function renderMenu(){
        //the menu main UL tag
        $r=$this->getOpenTagWrapper(); 
            $r.=$this->renderRoot();
        //the closure of the menu main UL
        $r.=$this->getCloseTagWrapper();
        return $r;  
    }
    
    protected function renderRoot(){
        $o = $this->entity->rootOptions();
        $n='';
        foreach ($o as $ri) {
            if ($ri->hasChildren()) {
             //Render the wrapped dropdown main menu
             $n.=$this->getMenuWithDropDownWrapper($ri);
            }
            else {
                //Render the wrapper without the submenu caret
               $n.=$this->getMenuWithoutDropdownWrapper($ri);
            }
        }
        return $n;
    }

    /**
     * {@inheritdoc }.
     */
    public function getOpenTagWrapper()
    {
        //This theme has no special treatment on the UL
        return PHP_EOL . '<ul>' . PHP_EOL;
    }

    /**
     * {@inheritdoc }.
     */
    public function getCloseTagWrapper()
    {
        return PHP_EOL . '</ul>' . PHP_EOL;
    }

    /**
     * {@inheritdoc }.
     */
    public function getMenuWithoutDropdownWrapper($item)
    {
        return '<li><a href="' . $item->getUrl() . '">'. $item->optionName.'</a></li>' . PHP_EOL;
        // return '<li' . $this->getActiveState($item) . '><a href="' . $item->getUrl() . '" ' . $item->getAttributes() . '>' . $item->getIcon() . ' ' . $item->title . '</a></li>' . PHP_EOL;
    }

    /**
     * {@inheritdoc }.
     */
    public function getActiveState($item, $state = ' class="active"')
    {
        return $item->isActive() ? $state : null;
    }

    /**
     * Get active state on child items.
     *
     * @param $item
     * @param string $state
     *
     * @return null|string
     */
    public function getActiveStateOnChild($item, $state = 'active')
    {
        return $item->hasActiveOnChild() ? $state : null;
    }

    /**
     * {@inheritdoc }.
     */
    public function getDividerWrapper()
    {
        return '<li class="divider"></li>';
    }

    /**
     * {@inheritdoc }.
     */
    public function getHeaderWrapper($item)
    {
        return '<li class="dropdown-header">' . $item->title . '</li>';
    }

    /**
     * {@inheritdoc }.
     */
    public function getMenuWithDropDownWrapper($item)
    {
        return '<li class="text-left">
		          <a href="'. $item->getUrl() . '">
					'. $item->optionName . '
			      	<b class="caret"></b>
			      </a>
			      <ul class="sub-menu">
                    ' . $this->getChildMenuItems($item) . ' 
			      </ul>
		      	</li>'
        . PHP_EOL;
    }
    // Cleanup later: implement the rest of the logic.
    // public function getMenuWithDropDownWrapper($item)
    // {
    //     return '<li class="dropdown' . $this->getActiveStateOnChild($item, ' active') . '">
	// 	          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	// 				' . $item->getIcon() . ' ' . $item->title . '
	// 		      	<b class="caret"></b>
	// 		      </a>
	// 		      <ul class="dropdown-menu">
	// 		      	' . $this->getChildMenuItems($item) . '
	// 		      </ul>
	// 	      	</li>'
    //     . PHP_EOL;
    // }

    /**
     * Get multilevel menu wrapper.
     *
     * @param \Nwidart\Menus\MenuItem $item
     *
     * @return string`
     */
    public function getMultiLevelDropdownWrapper($item)
    {
        return '<li class="text-left">
		          <a href="'. $item->getUrl() . '">
					 '.$item->optionName . '
			      <span class="fa fa-caret-right" id="right-caret">
                  </a>
                    <ul class="sub-menu">
	 		      	' . $this->getChildMenuItems($item) . '
	 		        </ul>
		      	</li>'
        . PHP_EOL;
    }

    // public function getMultiLevelDropdownWrapper($item)
    // {
    //     return '<li class="dropdown' . $this->getActiveStateOnChild($item, ' active') . '">
	// 	          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	// 				' . $item->getIcon() . ' ' . $item->title . '
	// 		      	<b class="caret pull-right caret-right"></b>
	// 		      </a>
	// 		      <ul class="dropdown-menu">
	// 		      	' . $this->getChildMenuItems($item) . '
	// 		      </ul>
	// 	      	</li>'
    //     . PHP_EOL;
    // }
}
