# Menubuilder by AndersonPEM

A database driven dynamic menu engine.

**v0.2**

This module is built upon the nwidart/laravel-modules package.

Developed by the need of creating menus and finding the existing packages not understandable enough for my projects.

## Features

The module supports generation, indexing and management of menus with unlimited levels of depth. For convenience, as there are a lot of database queries involved in the recursive process of the menu, this package comes with a cache helper listed in the dependencies list below.  You don't need to worry about the cache. Just set it right and let it roll :)
Among the features are:

 - Creation of multiple menus
 - Unlimited depth of the nested set. You can set a limit though.
 - Presenters to render your menus' tags according to your templating needs.
 - Roles and permissions check with Gate functionality and spatie/laravel-permission.

## Dependencies

This module currently requires the following packages:

 - The Laravel Modules package (nwidart/laravel-modules)
 - genealabs/laravel-model-caching (for easy model and custom query caching in a breeze)
 - laracasts/presenter (for formatting the menu output in presenters)
 - Laravel Module Installer (moves the modules to the Modules folder instead of vendor)

## Database Tables

Currently, setting the table names is still not available with the vanilla module. You'd have to change that in the code. Please make sure your database does not contain tables with the following names:

 - menus
 - menu_items
 - menu_item_types

The implementation for table name change is supposed to roll out in future updates, since I don't have a lot of time right now to move forward with the rest of the project I'm working on :)

## How to?

- Install nwidart/laravel-modules, set it up properly and then require this package:

```
   composer require andersonpem/menubuilder-module ">=0.2" 
```

- Publish the module config files with php artisan module:publish-config menubuilder
- Add the default presenter to the .env file or a custom presenter of yours: 
  
```env
    MENUBUILDER_PRESENTER=bootstrap
```
- Run the necessary migrations with php artisan migrate

- Generate a startup menu to play around with:

```
   //careful not to clear the entire database in case you already have data. The seeder will ask if you wanna clear the entire database.
   php artisan module:seed menubuilder --class=StartupSeeder 
```

- Render the demo menu in blade with:
```blade
{!!Menu::renderMenuById(1)!!}
```

Keep in mind MenuBuilder renders the ul tags of the menu alone. The pre and post div and nav elements have to be specified in your blade template :). That allows you to be a little more flexible.

## Todo

 - The implementation of other types beyond Link in the component logic
 - Documentation
 - Graphical management of the menus embedded in the module
 - ~~Move the development of the component to dev branch of main project.~~
 - Laravel Gate / Laravel Permission implementation
 - Active property mapping
 - Implement the depth throttling functionality
 - Customize the table names in config file
 - ~~Customize the presenter name via config file and .env file~~
 - Since we know which options are from which parent, maybe a breadcrumb renderer? The presenter classes would need some extra lines :)
 - And in the same line of thought, why not to develop a dynamic sitemap renderer for the site's public menu?

Something like:

```php
Menubuilder::renderSiteMap($menu, $method);
```

## Licensing

This program can be distributed following the MIT licensing. You can use it, fork it, create new things out of it, sell derivative products, but no liability shall be implied to the author and no guarantee of work is provided.
