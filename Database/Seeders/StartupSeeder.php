<?php

namespace Modules\Menubuilder\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Menubuilder\Database\Seeders\CreatesExampleMenuTableSeeder;
use Modules\Menubuilder\Database\Seeders\MenuItemTypesDefaultsTableSeeder;

class StartupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
             // Ask for db migration refresh, default is no
        if ($this->command->confirm('Would you like to clear the entire database before continuing?')) {

            $this->command->alert("Rolling back all migrations and rerunning");
            // Call the php artisan migrate:refresh using Artisan
            $this->command->call('migrate:refresh');
            $this->command->call('modelcache:clear');
            //Fuck. All data is gone. Hope you have some backup.
            $this->command->comment("Data cleared, starting from blank database.");
        }
        $this->command->alert(" Creating the data types and default menu   ");
        //Populate types table
        $this->call(MenuItemTypesDefaultsTableSeeder::class);
            if ($this->command->confirm('Create the library\'s default menu?')) {
                $this->command->alert(" Creating the example menu  ");
                //Create the example menu
                $this->call(CreatesExampleMenuTableSeeder::class);
        }
        $this->command->info('Proccess complete.');
        Model::reguard();
    }
}
