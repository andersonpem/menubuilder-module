<?php

namespace Modules\Menubuilder\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Menubuilder\Entities\MenuItemType;

class MenuItemTypesDefaultsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Model::unguard();
        $this->command->info('Creating the default types in the database...');
        $types = MenuItemType::defaults();
        foreach ($types as $type) {
            //Localiza o registro e ignora ou cria um novo
            MenuItemType::firstOrCreate(
                ['name' => $type['name'],
                'desc'=> $type['desc'],
                'factory'=> $type['factory'],
                ]);
        }
        $this->command->info('Created menu item types.');
        // MenuItemType::find(1);
        // $this->call("OthersTableSeeder");
        
        Model::reguard();
    }
}
