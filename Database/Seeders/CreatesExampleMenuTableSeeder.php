<?php

namespace Modules\Menubuilder\Database\Seeders;

use Menubuilder\Menu;
use Menubuilder\MenuItem;

use Menubuilder\MenuItemType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class CreatesExampleMenuTableSeeder extends Seeder
{
    /**
     * Performs a menu import.
     * I'll probably port this function to the main Menu Class. 
     * That way the user can backup it's menus and restore them from a JSON file.
     */
    protected function processMenu($options, $menuObject, $parent=null){
        $m = $menuObject;
        if (count($options)<1) {
            throw new \Exception('Seeding MenuItem error: no default options.');
        }
        if(empty($menuObject)){
            throw new \Exception('Seeding MenuItem error: no menu to seed to.');
        }
        //Run through the main options
        foreach ($options as $i) {
                $mi = MenuItem::firstOrCreate([
                'menuId'=>$m->id,
                'optionName'=> $i['optionName'],
                'typeId'=> $i['typeId'],
                'object'=> $i['object'],
                'parentId'=> $parent,
                'featured'=> false,
                ]);
                // Puts item as last in the list for indexing purposes
                $mi->comesLast();
                if ($this->hasChildren($i)) {    
                    // echo $mi->id.PHP_EOL;
                    //echo "fifi. ".$mi;
                    $this->processMenu($i['children'], $m, $mi->id);
                }
        }
    }
    protected function hasChildren($option){
        // (count($option['children'])>0) ? true : false;
        if(!empty($option['children'])){
            if (count($option['children'])>0) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
        
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard(); 
        $this->command->line("======================================================");
        $this->command->error("Wiping all data from Menu and MenuItem tables...");
        $this->command->line("======================================================");
        DB::statement("SET foreign_key_checks=0");
            Menu::truncate();
            MenuItem::truncate();
        DB::statement("SET foreign_key_checks=1");
        $this->command->comment('Creating the example menu of the library');
        Menu::unguard();
            $menu = new Menu;
            $menu->name = "Default Menu";
            $menu->desc = "This is the default menu from MenuBuilder by AndersonPEM.";
            $menu->active = true;
            $menu->save();
        Menu::reguard();
        //dd($menu);
        //Let's create items :)
        $this->command->comment('Recursively populating the menu');
        $this->processMenu(Menu::exampleMenu(), $menu);
        // now to make sure the ordering is proper, let's reindex everyone
        $this->command->line("=======================================");
        $this->command->line("Indexing the created menu items...");
        $this->command->line("=======================================");
        $menu->reindexLevels();
        $this->command->comment('Indexing complete.');
        $this->command->line("=======================================");
        $this->command->info('The menu was built. Have fun :)');
        $this->command->line("=======================================");
        
        Model::reguard();
    }
    
}
