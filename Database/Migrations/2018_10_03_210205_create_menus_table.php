<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            //Nome do Menu a ser instanciado
            $table->string('name')->nullable();
            $table->string('desc')->nullable();
            $table->boolean('active')->default(0);
            $table->integer('maxLevels')->default(2);
            $table->timestamps();
            
            //Enable in prod :)
            // $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
