<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menuId')->unsigned();
            $table->string('optionName');
            $table->integer('typeId')->unsigned();
            $table->string('object')->nullable();
            //Parent is a row in this very table. No foreign key constraint required. But let's unsign it to make sure ^^
            $table->integer('parentId')->unsigned()->nullable();
            $table->boolean('active')->default(1);
            //Special treat for featured options on CSS. You can emphasize an option which you want the user to notice.
            $table->boolean('featured')->default(0);
            //How deep (is your love?) this item is in the tree. Default is menu root (0)
            $table->integer('level')->default(0);
            //Incremental order in the level the item is. Order can be programatically changed by comesBefore() and comesAfter() functions in the Model.
            $table->integer('order')->nullable();
            //Ability and role implementation
            $table->json('can')->nullable();
            // Spatie/Laravel-Permission has an entity called roles. Sometimes it's easier to associate options to an array of roles.
            $table->json('roles')->nullable();
            //Foreign key constraints
            $table->foreign('menuId')
                  ->references('id')->on('menus')
                  ->onDelete('cascade');

            $table->foreign('typeId')
                  ->references('id')->on('menu_item_types')
                  ->onDelete('cascade');
            // $table->string('');

            $table->timestamps();
            //$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_items');
    }
}
