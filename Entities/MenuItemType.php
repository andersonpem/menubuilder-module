<?php

namespace Modules\Menubuilder\Entities;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
/**
 * Specify which type an object is. That can be useful for treating the data.
 */
class MenuItemType extends Model
{
    protected $fillable = ['name', 'desc'];
    use Cachable;
    /**
     * Returns the default options
     */
    public static function defaults(){
        return [
            ['name'=>'page', 'desc'=>'It\'s a page object', 'factory'=>true,],
            ['name'=>'link', 'desc'=>'Insert a fully qualified URL', 'factory'=>true,],
            ['name'=>'model', 'desc'=>'Model specification (to be implemented)', 'factory'=>true,],
            ['name'=>'text', 'desc'=>'Text, for when there is no link to content like in pointers to submenus', 'factory'=>true,],
        ];
    }
}
