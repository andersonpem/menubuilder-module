<?php

namespace Modules\Menubuilder\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Artisan;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

/** 
 * All your items are belong to us.
 * Items are the actual menu options. Items on the level 0 are the root of the tree.
 * Items below that are in the next levels, always identified by a parent.
 * I wonder if the leveling in the tree is required beyond the 0 which identifies the root.
 * Cause the others can be nested by parent-chilren relationships.
 * - This class is Cache able with Genealabs\LaravelModelCaching. For using it without caching,
 * disable the use clause above and the cachable trait below.
 * - Roles and permissions: permissions are checked in the Laravel Gate. Permissions are checked with Spatie/Laravel-permission. 
 * 
*/
class MenuItem extends Model
{
    use Cachable;
    protected $fillable = ['menuId', 'optionName','typeId','object','parentId','featured','level', 'order', 'can', 'roles'];
    
    public function menu(){
        return $this->belongsTo(Menu::class, 'menuId', 'id');
    }
    public function type(){
        return $this->belongsTo(MenuItemType::class, 'typeId', 'id');
    }
    
    // Event handlers
    public static function boot() {
        parent::boot();
        /**
         * As we are in a one table relationship, all children of the model being deleted are to perish with it. That way we keep consistency in the database. And also eliminate garbage.
         */
        static::deleting(function(MenuItem $mi) { // before delete() method call this
             if ($mi->hasChildren()) {
                 foreach ($mi->children() as $mc) {
                     $mc->delete();
                 }
             }
        });
    }



    /**
     * Parent is a recursive call to this same model. It will return whoever is the parent associated with another row :)
     * There can only be one father.
     */
    public function parent(){
        $p = $this->where('id',$this->parentId)->first();
        return $p;
    }
    /**
     * Children returns all the rows that are children of a specific model
     */
    public function children(){
        $r = $this->where([
            ['parentId', $this->id],
            ['active', true],
        ])->orderBy('order')->get();
        return $r;
    }
    /**
     * Returns true if the current instance of the model has active children of its own. Else, returns null or false.
     * Just to lower a little the repetition of if count in the code
     */
    public function hasChildren(){
        $r = $this->where([
            ['parentId', $this->id],
            ['active', true],
        ])->orderBy('order')->get();
        //Ternary statements. So cool :P
        return (count($r)>0) ? true : false ;
    }
    /**
     * Having a parent in the family is rare in Brazil :P
     * This function returns if the current model instance has a parent registered.
     */
    public function hasParent(){
        $r = $this->where([
            ['parentId','<>', $this->id],
            ['id','=', $this->id],
            ['parentId','<>', null],
            ['active', true],
        ])->get();
        //Ternary statements. So cool :P
        return (count($r)>0) ? true : false ;
        // return $r;
    }

    /** 
     * Sorts a collection of data. $element is sorted before $who in the collection.
     * The order column of the collection is reindexed according to the new element's position.
     * Made with lots of coffee by AndersonPEM.
     * Dunno if it's the best solution, but it seemed appropriate to me.
    */
    protected function sortBefore($who, $element, $collection){
        //Note: on closure functions access to the parent scope variables must be declared.
        $c=$collection;
            //First let's locate the $element on the collection
            $idThis=$c->search(function ($item, $key) use ($who, $element) {return $item->id == $element;});
            //Now we'll remove it from the collection, so we can work with it in order to put in the desired position.
            $thisElement = $c->splice($idThis,1);
            //Convert the element to a single eloquent model instance
            $thisElement=$thisElement->first();
            //We're done with the target element. let's locate $who's position in the collection.
            $idWho=$c->search(function ($item, $key) use ($who, $element) {return $item->id == $who;});
        //Located the element. Now let's splice the shit out of it back in place in the desired position.
        $c->splice($idWho, 0, [$thisElement]);
            //That's done. Now let's reindex the elements in the current order of the Collection.
            $i=0;
            //Incrementally creates the order and persist the eloquent models' changes back to the database.
            foreach ($c->all() as $key => $item) { 
                $item->order=$i; $item->save(); $i++;
            }
        //Such a good boy. Done.
        return true;
    }
    /** 
     * Sorts a collection of data. $element is sorted after $who in the collection.
     * The order column of the collection is reindexed according to the new element's position.
     * Made with lots of coffee by AndersonPEM.
     * Dunno if it's the best solution, but it seemed appropriate to me.
    */
    protected function sortAfter($who, $element, $collection){
        //Note: on closure functions access to the parent scope variables must be declared.
        $c=$collection;
            //First let's locate the $element on the collection
            $idThis=$c->search(function ($item, $key) use ($who, $element) {return $item->id == $element;});
            //Now we'll remove it from the collection, so we can work with it in order to put in the desired position.
            $thisElement = $c->splice($idThis,1);
            //Convert the element to a single eloquent model instance
            $thisElement=$thisElement->first();
            //We're done with the target element. let's locate $who's position in the collection.
            $idWho=$c->search(function ($item, $key) use ($who, $element) {return $item->id == $who;});
            //Since we're putting after $who, let's increment idWho in one :)
            $idWho++;
        //Located the element. Now let's splice the shit out of it back in place in the desired position.
        $c->splice($idWho, 0, [$thisElement]);
            //That's done. Now let's reindex the elements in the current order of the Collection.
            $i=0;
            //Incrementally creates the order and persist the eloquent models' changes back to the database.
            foreach ($c->all() as $key => $item) { 
                $item->order=$i; $item->save(); $i++;
            }
        //Such a good boy. Done.
        return true;
    }

    /**
     * The comes before function validates the information required to confirm the order.
     * If successful it changes the element position on the collection and reindexes the order values of the collection.
     */
    public function comesBefore($who){
        // Let's check if it's in the same level and if it's not zero, if they belong to the same parent
        $m = MenuItem::findOrFail($who);
        if ($m->level == $this->level) {
            // we're on the same level, people.
            if ($m->level<>0) {
                //Checking if both belong to the same parent
                    if ($m->parent() == $this->parent()) {
                        //Proceed.
                        //Here we have to consider (on the query individuals with the same parent)
                        $l = MenuItem::where([
                            ['level', '=', $this->level],
                            ['parentId', '=', $this->parent()->id],
                        ])->get();
                        return $this->sortBefore($who, $this->id, $l);
                    
                    }
                    else {
                        //Error. Different parents. It's a brazilian marriage :P
                        throw new \Exception('MenuItem: Items have different parents.');
                    }
                }
            else {
            //We're in the root level of the tree.
                //Let's find everyone in the root and reindex them according to specifications.
                $l = MenuItem::where('level', $this->level)->get();
                // echo $this->id;
                // die;
                return $this->sortBefore($who, $this->id, $l);
            }
            

        } else {
            //Error - Items are in different levels.
            throw new \Exception('MenuItem: You\'re trying to associate items of different levels.');
        }

    }
    /**
     * The comesAfter function validates the information required to confirm the order.
     * If successful it changes the element position on the collection and reindexes the order values of the collection.
     */
    public function comesAfter($who){
        // Let's check if it's in the same level and if it's not zero, if they belong to the same parent
        $m = MenuItem::findOrFail($who);
        // dd($m);
        if ($m->level == $this->level) {
            // we're on the same level, people.
            if ($m->level<>0) {
                //Checking if both belong to the same parent
                    if ($m->parent() == $this->parent()) {
                        //Proceed.
                        //Here we have to consider (on the query individuals with the same parent)
                        $l = MenuItem::where([
                            ['level', '=', $this->level],
                            ['parentId', '=', $this->parent()->id],
                        ])->get();
                        return $this->sortAfter($who, $this->id, $l);
                    
                    }
                    else {
                        //Error. Different parents. It's a brazilian marriage :P
                        throw new \Exception('MenuItem: Items have different parents.');
                    }
                }
            else {
            //We're in the root level of the tree.
                //Let's find everyone in the root and reindex them according to specifications.
                $l = MenuItem::where('level', $this->level)->get();
                return $this->sortAfter($who, $this->id, $l);
            }
            

        } else {
            //Error - Items are in different levels.
            throw new \Exception('MenuItem: You\'re trying to associate items of different levels.');
        }

    }

public function comesFirst(){
    //Let's catch the list of everyone in the same level as the current model.
    $element = $this->id;
    $l = MenuItem::where('level', $this->level)->orderBy('order')->get();
    //Now let's find the 
    $idThis=$l->search(function ($item, $key) use ($element) {return $item->id == $element;});
    
    //Now we'll remove it from the collection, so we can work with it in order to put in the desired position.
    $thisElement = $l->splice($idThis,1);
    //Convert the element to a single eloquent model instance
    $thisElement=$thisElement->first();
    //Now we just prepend the element, which goes to the beginning of the collection :)
    $l->prepend($thisElement);
    //That's done. Now let's reindex the elements in the current order of the Collection.
        $i=0;
        //Incrementally creates the order and persist the eloquent models' changes back to the database.
        foreach ($l->all() as $key => $item) { 
            $item->order=$i; $item->save(); $i++;
        }
    //Such a good boy. Done.
    return true;
}
/**
 * This function places the current eloquent model as the last one in the menu list and reindexes the item's level accordingly.
 */
public function comesLast(){
    //Let's catch the list of everyone in the same level as the current model.
    $element = $this->id;
    $p = ($this->parent()) ? $this->parent()->id : null;
    $lvl = ($this->level) ? $this->level : 0;
    $l = MenuItem::where([
                            ['level', $lvl],
                            ['parentId', $p],
                            ])->orderBy('order')->get();
    //Now let's find the element in the array
    $idThis=$l->search(function ($item, $key) use ($element) {return $item->id == $element;});
    
    //Now we'll remove it from the collection, so we can work with it in order to put in the desired position.
    $thisElement = $l->splice($idThis,1);
    //Convert the element to a single eloquent model instance
    $thisElement=$thisElement->first();
    //Now we just push the element, which goes to the end of the collection :)
    $l->push($thisElement);
    //That's done. Now let's reindex the elements in the current order of the Collection.
        $i=0;
        //Incrementally creates the order and persist the eloquent models' changes back to the database.
        foreach ($l->all() as $key => $item) { 
            $item->order=$i; $item->save(); $i++;
        }
    //Such a good boy. Done.
    return true;
}

/**
 * Sets the parent of an element.
 * Sets element as member of root of tree if $who is null
 */
public function setParent($who){
    if(!empty($who)){
        //Prevents infinite recursive loops on the tree building
        if ($who==$this->id) {
            throw new \Exception('MenuItem: You\'re trying to do a paradoxal relationship between a menu item and itself. This database is not quantic yet. ^^');
        }
        $this->parentId=$who;
        $this->save();
        //When changing one's parent, we have to reindex the level of the menus so we keep consistency.
        $this->menu->first()->reindexLevels();
        //Also, put as last one of the new level. This way the ordering keeps populated and there is no conflict with other items of the same new level in case one of them has the same ordering number.
        $this->comesLast();
        return true;
    }
    else {
        //Make the model a member of the root tree, puts it as last and reindex the tree for keeping the order values populated.
        $this->parentId=null;
        //$this->order=null;
        $this->parentId=null;
        $this->level=0;
        $this->save();
        $this->comesLast();
        return true;
    }

}
/**
 * Returns wether an item is enabled to show or not for the menu rendering.
 */
public function hidden(){
    return !$this->active;
}

public function getUrl(){
    return $this->object;
}
public function isHeader(){
    return false;
}
public function isDivider(){
    return false;
}

}
