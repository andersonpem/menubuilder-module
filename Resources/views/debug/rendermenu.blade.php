@extends('menubuilder::layouts.master')

@section('content')
    <!--Create pre and post elements of navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
          <!--The container of navbar itself -->
          <div id="navbar" class="navbar-collapse collapse">
                <!--Ask the menu to render -->
                {!!$menu->present()->renderMenu()!!}
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
@stop
